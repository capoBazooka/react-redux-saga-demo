function* infiniteGenerator() {
    let i = 1;

    while(true) {
        yield i++;
    }
}

function* finiteGenerator() {
    let i;
    for(i = 0; i < 5; i++) {
        yield i + 2;
    }
}


let nextId = infiniteGenerator();

for(let i = 0; i < 10; i++) {
    const current = nextId.next();

    console.log(`Id: ${current.value}. Finalizado? ${current.done}`);

}