import { useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

function ToDoItem({TaskId, TaskTimestamp, TaskTitle, TaskCompleted, checkHandler, deleteHandler}) {

    const [checkState, setCheckState] = useState(TaskCompleted)

    const handleDelete = (e) => {
        e.preventDefault();

        deleteHandler(TaskId);
    }

    const handleCheck = (e) => {
        setCheckState(!checkState);
        checkHandler(TaskId, !checkState);
    }


    return (
        <Row>
            <Col>
                <Form>
                    <Row>
                        <Col xs={11}>
                        <Form.Check type="checkbox" id={TaskId} checked={checkState} label={TaskTitle} onChange={handleCheck} />
                        </Col>
                        <Col xs={1}>
                            <Button type="submit" onClick={handleDelete}>Borrar</Button>
                        </Col>
                    </Row>
                </Form>
            </Col>
        </Row>
    )
}

ToDoItem.propTypes = {
    TaskId: PropTypes.string,
    TaskTimestamp: PropTypes.string,
    TaskTitle: PropTypes.string,
    TaskCompleted: PropTypes.bool,
    disabled: PropTypes.bool,
    deleteHandler: PropTypes.func.isRequired,
    checkHandler: PropTypes.func.isRequired
}

export default ToDoItem;
