import '../App.css';
import { Container, Card, ListGroup } from 'react-bootstrap';

import ToDoFrom from './todo-form';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, { useEffect, useState } from 'react';
import ToDoItem from './todo-item';

import { API_ENDPOINT } from '../constants/Common';


function ToDo() {

  const [toDoList, setToDoList] = useState([]);

  useEffect(() => {
    fetch(API_ENDPOINT,
    {
      method: "get"
    })
    .then(res => res.json())
    .then(response => {
      response.sort((a, b) => a.timestamp > b.timestamp ? -1 : 1)
      setToDoList(response);
    })
    .catch(err => console.log(err))
  }, []);


  // Aca el useEffect para escuchar cambios en la lista

  
  function handleAddToDo(taskTitle) {
    const newTodo = {
      title: taskTitle
    };

    // Primer intento: usar fetch
    fetch(API_ENDPOINT,
    {
      method: "post",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newTodo)
    })
    .then(res => res.json())
    .then(response => {
      setToDoList(() => [response, ...toDoList]);
    })
    .catch(err => console.log(err.message));
  }

  function handleBorrar(taskId) {

    fetch(API_ENDPOINT + "/" + taskId,
    {
      method: "delete",
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(response => {
      setToDoList(toDoList.filter(task => task.id !== response.id));
    });
  }

  function handleToDoCheck(taskId, taskStatus) {

    console.log(taskStatus);

    let taskIndex = toDoList.findIndex(item =>  item.id === taskId);
    const task = toDoList[taskIndex];
    task.completed = taskStatus;

    fetch(API_ENDPOINT + "/" + taskId,
    {
      method: "put",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(task)
    })
    .then(res => res.json())
    .then(response => {
      let newList = [...toDoList];
      newList[taskIndex] = response;
    })
    .catch(err => console.log(err.message));

    
    console.log(toDoList[taskIndex]);
  }
  
  return (
    <Container xs={12} md={8} lg={6} className="h-100 align-middle">

      <Card className="mt-5 mb-5 align-middle">
        <Card.Body>
          <Card.Title className="text-align-center">Existen {toDoList.length || 0} {(toDoList.length || 0) === 1 ? "tarea" : "tareas" } en la lista</Card.Title>
            <ToDoFrom submitHandler={handleAddToDo}></ToDoFrom>
            <ListGroup key="ToDo" variant="flush">
              {toDoList.map( toDo => <ListGroup.Item key={toDo.id}><ToDoItem TaskId={toDo.id} TaskTitle={toDo.title} checkHandler={handleToDoCheck} TaskCompleted={toDo.completed} deleteHandler={handleBorrar}></ToDoItem></ListGroup.Item> )}
            </ListGroup>
        </Card.Body>
      </Card>

    </Container>
  );
}

export default ToDo;
