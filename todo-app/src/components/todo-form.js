import { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form } from 'react-bootstrap';

function ToDoForm(props) {
    const [text, setText] = useState(props.text || "");

    const handleTextChange = (e) => {
        setText(e.target.value);
    }

    const handleTaskSubmit = (e) => {
        e.preventDefault();

        const taskTitle = text.trim();

        if(taskTitle === "") {
            return alert("Ingrese una tarea valida!")
        }

        props.submitHandler(taskTitle);

        setText("");
    }

    return (
        <Row>
            <Col>
                <Form onSubmit={handleTaskSubmit}>
                    <Form.Group controlId="todoName">
                        <Row>
                            <Col xs={12} className="mt-2">
                                <Form.Control 
                                    className="form-control-lg"
                                    type="input"
                                    placeholder="Ingrese la tarea a realizar..."
                                    value={text}
                                    onChange={handleTextChange}
                                />
                            </Col>
                        </Row>
                    </Form.Group>
                </Form>
            </Col>
        </Row>
    )
}

ToDoForm.propTypes = {
    text: PropTypes.string,
    disabled: PropTypes.bool
}


export default ToDoForm;