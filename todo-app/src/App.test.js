import { render, screen } from '@testing-library/react';
import ToDo from '.components/ToDo';

test('renders learn react link', () => {
  render(<ToDo />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
